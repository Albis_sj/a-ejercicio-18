import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1padre = {
    nombre: "Aquaman",
    aparicion: "1941-11-01",
    casa:"DC"
  };

  Objeto2padre = {
    desayuno: 'Cafe',
    almuerzo: 'bife',
    cena: 'trancapecho'
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
